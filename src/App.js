import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";

import NavBar from "./components/navbar";
import Home from "./components/Home/home";
import Missions from "./components/Missions/missionPage";

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar
                  list={<List>
                    <ListItem button component={Link} to='/missions'>
                        <ListItemText primary="Missions"></ListItemText>
                    </ListItem>
                  </List>}
            routing={
                    <Switch>
                      <Route exact path='/' component={Missions} />
                      <Route path = '/missions' component={Missions} />
                    </Switch>
                    }
        />
      </Router>
    </div>
  );
}

export default App;
