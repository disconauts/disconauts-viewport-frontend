import React from 'react';
import FitsViewer from './fitsImage';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import { Container, Row, Col } from 'reactstrap';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import UrlList from "./urlList";
import axios from 'axios';

const useStyles = makeStyles({
    date: {
      width: 500
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  });

  export default class Mission extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        day: 0,
        month: 0,
        year: 0,
        urls: [ ]
      };
  
      this.handleChangeMonth = this.handleChangeMonth.bind(this);
      this.handleChangeDay = this.handleChangeDay.bind(this);
      this.handleChangeYear = this.handleChangeYear.bind(this);
    }

    handleChangeMonth(e){
      this.setState({ month: e.target.value })
    }

    handleChangeDay(e){
      this.setState({ day: e.target.value })
    }

    handleChangeYear(e){
      this.setState({ year: e.target.value })
    }

    search = () => {
      axios.get('http://nasa.dudelab.net/api/Swift?year=' + this.state.year + '&month=' + this.state.month + '&day=' + this.state.day)
            .then(res => {
                this.setState({ urls: res.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    check = () => {
      if(this.state.urls.length == 0){
        alert("Still loading data...");
        console.log(this.state.urls);
      } else {
        alert("Data loaded. Click next button or reload to view data!")
        console.log(this.state.urls);
      }
    }

    render(){
      return (    
        <Container>
          <Row>
              <Col>
                  <Card>
                      <CardContent>
                        <FitsViewer urls={this.state.urls}></FitsViewer>
                      </CardContent>
                  </Card>
              </Col>
          </Row>
          <Row>
              <Col xs="6" sm="4"></Col>
              <Col xs="6" sm="4">
              <form onSubmit={this.handleSubmit}>
          <label>
            Month
            <input type="number" value={this.state.month} onChange={this.handleChangeMonth} />
          </label>
          <label>
            Day
            <input type="number" value={this.state.day} onChange={this.handleChangeDay} />
          </label>
          <label>
            Year
            <input type="number" value={this.state.year} onChange={this.handleChangeYear} />
          </label>
          <button type="button" onClick={this.search}>Search</button>
          <button type="button" onClick={this.check}>Check</button>
        </form>
              </Col>
          </Row>
      </Container>
      );
    }
}
