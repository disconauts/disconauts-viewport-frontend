import React from 'react';

export default class DateForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value: ''};
  
      this.handleChangeMonth = this.handleChangeMonth.bind(this);
      this.handleChangeDay = this.handleChangeDay.bind(this);
      this.handleChangeYear = this.handleChangeYear.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    onSubmit(event) {
      event.preventDefault();

      
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <label>
            Month
            <input type="number" value={this.state.month} onChange={this.handleChange} />
          </label>
          <label>
            Day
            <input type="number" value={this.state.day} onChange={this.handleChange} />
          </label>
          <label>
            Year
            <input type="number" value={this.state.year} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      );
    }
  }