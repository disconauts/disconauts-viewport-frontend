import React, {Component} from "react";
import { render } from "react-dom";
import ReactScript from 'react-script-tag'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

export default class FitsViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            count: 0,
            url: ""
    };
    this.refreshImage = this.refreshImage.bind(this);
    this.next = this.next.bind(this);
}

  componentDidMount(){
    var uri = "http://nasa.dudelab.net/api/FITSUtil?url=https://heasarc.gsfc.nasa.gov/FTP/swift/data/obs/2020_09/00013499039/xrt/products/sw00013499039xpc_sk.img.gz";
    var res = encodeURI(uri);
    this.setState({ url: "http://nasa.dudelab.net/web/fits.html?link=" + res })
    //this.setState({ url: "http://localhost:3000/fits.html?link=http://localhost:3000/imageFile.img" })
  }

  refreshImage = () => {
    var uri = "http://nasa.dudelab.net/api/FITSUtil?url="+this.state.url;
    var res = encodeURI(uri);
    this.setState({ url: "http://nasa.dudelab.net/web/fits.html?link=" + res })
    this.reload();
  }

  next = () => {
    if(this.props.urls.length == 0){
        alert("Please search a date to load data first!")
    } else {
        this.state.count = this.state.count + 1;
        this.state.url = this.props.urls.results[this.state.count].image_url;
        this.refreshImage();
        console.log("next");
    }
  }

  prev = () => {
      if(this.state.count == 0){
          alert("You are viewing the first image, please select next!");
      } else {
          this.state.count = this.state.count - 1;
          this.state.url = this.props.urls.results[this.state.count].image_url;
          this.refreshImage();
          console.log("prev");
      }
  }

  reload = () => {
    this.setState({
      index: this.state.index + 1
    });
  }

  render() {
    return (
      <div style={styles}>
        <iframe width="1000px" height="550px" key={this.state.index} title="title" src={this.state.url} />
        <Card>
        <button onClick={this.prev}>Prev</button><button onClick={this.reload} >Reload</button><button onClick={this.next}>Next</button>
        </Card>
        <Card>
            <CardContent>
                Objects: {this.props.urls.length == 0 ? "none" : this.props.urls.results[this.state.count].target_name}
            </CardContent>
        </Card>
        <Card>
            <CardContent>
                Time: {this.props.urls.length == 0 ? "none" : this.props.urls.results[this.state.count].month + "/" + this.props.urls.results[this.state.count].day + "/" + this.props.urls.results[this.state.count].year + " " + this.props.urls.results[this.state.count].hour + ":" + this.props.urls.results[this.state.count].minutes + ":" + this.props.urls.results[this.state.count].seconds}
            </CardContent>
        </Card>
      </div>
    );
  }
}