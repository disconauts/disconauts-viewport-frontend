import React from 'react';

import axios from 'axios';

export default class UrlList extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        files: [],
    }

    this.showState = this.showState.bind(this);
  }

  componentDidMount() {
    const file = this.loadFile("nasa.dudelab.net/api/FITSUtil?url=https://heasarc.gsfc.nasa.gov/FTP/swift/data/obs/2020_09/00013648009/xrt/products/sw00013648009xpc_ex.img.gz")
    this.setState({ files: file });
    console.log(file)
    
  }

  async loadFile(filePath) {
    let response = await fetch(filePath);
    let data = await response.blob();
    let metadata = {
        type: ''
    };
    let file = new File([data], "test.img", metadata);
    return file
}

  showState(){
      console.log(this.state);
  }

  render() {
    return (
      <div>
          <button>Prev</button>
          <button>Next</button>
      </div>
    )
  }
}